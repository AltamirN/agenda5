﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Agenda6.Models
{
    public class Pessoa
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Nome { get; set; }

        public string Email { get; set; }

        public virtual List<Telefone> Telefones { get; set; }
    }
}