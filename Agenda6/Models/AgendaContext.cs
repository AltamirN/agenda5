﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Agenda6.Models
{
    public class AgendaContext : DbContext
    {
        public AgendaContext() : base("Agenda")
        {

        }
        public DbSet<Pessoa> Pessoas { get; set; }
        public DbSet<Telefone> Telefones { get; set; }
    }
}