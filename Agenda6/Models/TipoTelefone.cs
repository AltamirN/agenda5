﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agenda6.Models
{
    public enum TipoTelefone
    {
        Residencial,
        Comercial,
        Celular,
        Recado
    }
}